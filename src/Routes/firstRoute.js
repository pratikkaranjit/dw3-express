import {Router} from "express"

let firstRouter = Router()

firstRouter
.route("/admin")
.get((req,res) => {
    res.json("I am admin, method is get")
})
.post((req,res) => {
    res.json("I am admin, method is post")
})
.patch((req,res) => {
    res.json("I am admin, method is patch")
})
.delete((req,res) => {
    res.json("I am admin, method is delete")
})

firstRouter
.route("/admin/name")

.get((req,res) => {
    res.json("I am admin, name is get")
})

.post((req,res) => {
    res.json("I am admin, name is post")
})

.patch((req,res) => {
    res.json("I am admin, name is patch")
})

.delete((req, res) => {
    res.json("I am admin, name is delete")
})

export default firstRouter