import { Router } from "express";


let categoryRouter = Router()

categoryRouter
.route("/")
.post((req,res)=> {
    res.json("I am admin, method is post")
})
.get((req, res) => {
    res.json("I am admin, method is get")
})
.patch((req, res) => {
    res.json("I am admin, method is patch")
})
.delete((req, res) => {
    res.json("I am admin, method is delete")
}) 

export default categoryRouter