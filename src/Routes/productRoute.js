import { Router } from "express";
import { HttpStatus } from "./config/constant.js";


let productRouter = Router()

productRouter
.route("/")
.post((req,res)=> {
    console.log(req.body)
    res.json("I am admin, method is post")
})
.get((req, res) => {
    res.json("I am admin, method is get")
})
.patch((req, res) => {
    res.json("I am admin, method is patch")
})
.delete((req, res) => {
    res.json("I am admin, method is delete")
}) 


productRouter
.route("/:id")
.get((req,res,next) => {
    console.log(("I am first"))
    res.status(HttpStatus.OK).json("get successfully")
    next();
}, 
(req,res,next) => {
    console.log(("I am second"))
})
export default productRouter
