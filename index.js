
import firstRouter from './src/Routes/firstRoute.js';
import productRouter from './src/Routes/productRoute.js';
import categoryRouter from './src/Routes/categoryRoute.js';
import express, {json} from "express";
import { HttpStatus } from './src/Routes/config/constant.js';

let app = express();
let port = 8000;

app.use(json())

app.use((req,res, next) => {
    console.log("I am first app middleware");
    // res.status(HttpStatus.OK).json("get successfully")
    next();
})
// ,
// (req,res, next) => {
//     console.log("I am second app middleware");
//     next(error);
// })

app.use("/", firstRouter)
app.use("/products", productRouter)
app.use("/category", categoryRouter);

// app.use((err,req, res, next) => {
//     console.log("to use error middleware");
// })

app.listen(port, () => {
    console.log(`my app is listening at port ${port}`);
});